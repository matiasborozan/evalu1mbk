/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package root.model;


public class Evalu1 {
    private float capital;
    private float tasaianual;
    private float numeroanos;
    
    public float getCapital(){
        return capital;
    }
    
    public void setCapital(float capital){
        this.capital = capital;
    }
    
    public float getTasaianual() {
        return tasaianual;
    }
    
    public void setTasaianual(float tasaianual){
        this.tasaianual = tasaianual;
    }
    public float getNumeroanos() {
        return numeroanos;
    }
    
    public void setNumeroanos(float numeroanos){
        this.numeroanos = numeroanos;
    }
}
