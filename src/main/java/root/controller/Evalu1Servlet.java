/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package root.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.Evalu1;

/**
 *
 * @author reing
 */
@WebServlet(name = "Evalu1Servlet", value = "/carcular")
public class Evalu1Servlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Evalu1 calculo = new Evalu1();
        calculo.setCapital(Float.parseFloat(request.getParameter("capital")));
        calculo.setTasaianual(Float.parseFloat(request.getParameter("tasaianual")));
        calculo.setNumeroanos(Float.parseFloat(request.getParameter("numeroanos")));
        
        float resultado = 0;
        
        resultado = calculo.getCapital() * (calculo.getTasaianual()/100)*calculo.getNumeroanos();
        
        request.setAttribute("calculo",calculo);
        request.setAttribute("resultado",resultado);
        
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
